import { useState,useEffect } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
 const [name, setname] = useState("")
  const [showInput, setshowInput] = useState(false)
  const [selectedOption, setSelectedOption] = useState("");
  const [value1, setvalue1] = useState(" ")
  const [value2, setvalue2] = useState()
  const [answer, setanswer] = useState("")
  const [myMap, setMyMap] = useState(new Map([['myArg',false]]));
  const [dis, setdis] = useState(false)
  

const handleSelectChange = (e) => {
  setSelectedOption(e.target.value);}
 
  const handleSelect=(key,e)=>{

    if(e.target.value==='false'){
      const updatedMap = new Map(myMap); // Create a copy of the map
      updatedMap.set(key,false); 
      setMyMap(updatedMap); }

    else if(e.target.value==='true'){
    const updatedMap = new Map(myMap); // Create a copy of the map
      updatedMap.set(key,true); 
      setMyMap(updatedMap);}
    
   setshowInput(false)
  }

  const handleDelete = (key) => {
    const updatedMap = new Map(myMap); // Create a copy of the map
    updatedMap.delete(key); // Delete the key from the copy
    
    setMyMap(updatedMap); // Update the state with the updated map
  };
  useEffect(() => {
    if (selectedOption === '4') {
      setanswer(value1 || value2);
    }
    if (selectedOption === '3') {
      setanswer(value1 && value2);
    }
    if (selectedOption === '1') {
      setanswer( value1.toString());
    }
    console.log(value1);
  }, [value1, value2, selectedOption,myMap]);

  console.log(myMap)
  return (
    <div>
      { Array.from(myMap.entries()).map(([key, value])=>
    <div className='d-flex my-2' key={key}>
    <div className="input-group input-group-sm    mx-3">
  <input type="text" className="form-control" value={key} onChange={(e)=>setname(e.target.value)} aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"/>
</div>
<select  className="form-select form-select-sm" onChange={(e)=>handleSelect(key,e)}  >
<option  value="">Select value</option>
  <option value="false">False</option>
  <option value="true">True</option>
</select>
<button className='btn btn-light mx-2' onClick={() => handleDelete(key)}>X</button>
</div>)}
<div className={`d-flex my-2 ${showInput?'d-block':'d-none'}`}>
    <div className="input-group input-group-sm    mx-3">
  <input type="text" className="form-control"  onChange={(e)=>setname(e.target.value)} aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"/>
</div>
<select  className="form-select form-select-sm" onChange={ (e)=>handleSelect(name,e)} >
  <option value="">Select value</option>
  <option value="false">False</option>
  <option value="true">True</option>
</select>
</div>
<button className='btn btn-light my-2' onClick={()=>setshowInput(!showInput)}>+ add arg </button>

<div>
    <select className="form-select my-3" value={selectedOption} onChange={handleSelectChange} >
      <option disabled value="">Open this select menu</option>
      <option value="1">argument</option>
      <option value="2">constant</option>
      <option value="3">and</option>
      <option value="4">or</option>
    </select>

    {(selectedOption === "1" ) && (
      // Render additional options for "argument" selected
      <select  className="form-select form-select-sm my-2" onClick={()=>setdis(true)} onChange={ (e)=>setvalue1(()=>myMap.get(e.target.value))} >
            <option disabled={dis} value="">Select Value</option>
      {  Array.from(myMap.entries()).map(([key, value])=>
<option  key={key} value={key}>{key}</option>

)}
</select>
    )
    
    }

    {selectedOption === "2" && (
      // Render additional options for "constant" selected
      <select  className="form-select form-select-sm" onChange={ (e)=>setanswer(e.target.value)} >
  <option value="">Select value</option>
  <option value="false">False</option>
  <option value="true">True</option>
</select>
    )}

    {(selectedOption === "3" ||selectedOption === "4") && (
      // Render additional options for "and" selected
      <div>
        <select  className="form-select form-select-sm my-2" onChange={ (e)=>setvalue1((e.target.value))} >
        {  Array.from(myMap.entries()).map(([key, value])=>
  <option key={key} value={value}>{key}</option>
 
)}
</select>

        <select  className="form-select form-select-sm my-2" onChange={ (e)=>setvalue2((e.target.value))} >
        {  Array.from(myMap.entries()).map(([key, value])=>
  <option  key={key} value={value}>{key}</option>
 
)}
</select>
      </div>
    )}

   
  </div>
    <h3>Result :{answer} </h3>
</div>
  )
}

export default App
